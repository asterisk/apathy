<blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr">Is there a browser extension that monochromes social media sites and major news/media outlets? Or just any site I tell it to monochrome?</p>
&mdash; Matt J. Sorenson, ND (@emjayess) <a href="https://twitter.com/emjayess/status/798640112770781185">November 15, 2016</a>
</blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>